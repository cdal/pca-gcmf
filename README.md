# pca-gCMF
------------------------------

# *Prerequisite*
-------------------------------
Please install R library:
library(CMF)
library(pROC)
library(data.table)
library(igraph)
library(sna)

Command
--------------------------
Rscript pca_gCMF.R matrix_list_pca.txt data/ outputFolder/

matrix_list_pca.txt file contains:
                1st column: name list of input matrices.
                2nd column: binary value {1,0} whether the input martrix file required PCA transformation.
                3rd column: entity type (index number) corresponds to the row in the matrix (entity type row index).
                4th column: entity type (index number) corresponds to the column in the matrix (entity type column index).
data/ folder contains a list of files listed in the first argument
outputFolder/ is your output folder ended with slash /

All columns are separated by tab delimited.
